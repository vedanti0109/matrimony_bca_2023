import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:matrimony/database/demo_database.dart';
import 'package:matrimony/model/user_model.dart';
import 'package:matrimony/view/add_user.dart';

class UserListPage extends StatefulWidget {
  @override
  State<UserListPage> createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  DemoDatabase db = DemoDatabase();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text(
          'Add User',
          style: TextStyle(color: Colors.white),
        ),
        actions: [
          InkWell(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) {
                  return AddUser();
                },
              )).then((value) {
                if (value != null) setState(() {});
              });
            },
            child: Column(
              children: [
                Icon(
                  Icons.add,
                  color: Colors.white,
                ),
                Text(
                  'Add User',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
      body: FutureBuilder<List<UserModel>>(
        future: db.getDataFromTblUser(),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            return AddUser(
                              name: snapshot.data![index].Name.toString(),
                              city: snapshot.data![index].City.toString(),
                              id: snapshot.data![index].UserID,
                            );
                          },
                        ),
                      ).then((value) {
                        if (value) setState(() {});
                      });
                    },
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Container(
                              color: snapshot.data![index].Name
                                          .toString()
                                          .characters
                                          .first
                                          .toLowerCase() ==
                                      'R'.toLowerCase()
                                  ? Colors.red
                                  : Colors.green,
                              height: 20,
                              width: 6,
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Text(
                                  snapshot.data![index].Name.toString(),
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                showCupertinoDialog(
                                  context: context,
                                  builder: (context) {
                                    return CupertinoAlertDialog(
                                      title: Text(
                                        'Are you sure want to delete?',
                                      ),
                                      actions: [
                                        TextButton(
                                          onPressed: () async {
                                            await db.deleteUserFromTbleUser(
                                                userId: snapshot
                                                    .data![index].UserID);
                                            Navigator.pop(context);
                                            setState(() {});
                                          },
                                          child: Text(
                                            'Yes',
                                          ),
                                        ),
                                        TextButton(
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          child: Text(
                                            'No',
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                              child: Icon(
                                Icons.delete,
                                color: Colors.red,
                              ),
                            ),
                            Icon(
                              Icons.keyboard_arrow_right,
                              color: Colors.grey,
                            )
                          ],
                        ),
                      ),
                      margin: EdgeInsets.all(5),
                      elevation: 10,
                    ),
                  );
                },
              ),
            );
          } else {
            return const CircularProgressIndicator();
          }
        },
      ),
    );
  }
}
