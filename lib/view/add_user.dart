import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:matrimony/database/demo_database.dart';

class AddUser extends StatefulWidget {
  String? name, city;
  int? id;

  AddUser({this.name, this.id, this.city});

  @override
  State<AddUser> createState() => _AddUserState();
}

class _AddUserState extends State<AddUser> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController nameController = TextEditingController();

  TextEditingController cityController = TextEditingController();

  DemoDatabase db = DemoDatabase();

  @override
  void initState() {
    super.initState();
    nameController.text = widget.name ?? '';
    cityController.text = widget.city ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text(
          'Add User',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                controller: nameController,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Enter Valid Name';
                  }
                },
                decoration: InputDecoration(
                  hintText: 'Enter Name',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                controller: cityController,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Enter Valid City';
                  }
                },
                decoration: InputDecoration(
                  hintText: 'Enter City',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
              TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(Colors.blue),
                ),
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    int id;
                    if (widget.id == null) {
                      id = await db.insertUserInTblUser(
                        name: nameController.value.text,
                        city: cityController.value.text,
                      );
                    } else {
                      id = await db.updateUserInTblUser(
                        name: nameController.value.text,
                        city: cityController.value.text,
                        userId: widget.id!,
                      );
                    }

                    if (id <= 0) {
                      showCupertinoDialog(
                        context: context,
                        builder: (context) {
                          return CupertinoAlertDialog(
                            content: Text('Some Error Occured'),
                            actions: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text('Ok'))
                            ],
                          );
                        },
                      );
                    } else {
                      showCupertinoDialog(
                        context: context,
                        builder: (context) {
                          return CupertinoAlertDialog(
                            content: Text('Data Saved Successfully'),
                            actions: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                    Navigator.pop(context, true);
                                  },
                                  child: Text('Ok'))
                            ],
                          );
                        },
                      );
                    }
                  }
                },
                child: Text(
                  'Submit',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
