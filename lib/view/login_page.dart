import 'package:flutter/material.dart';
import 'package:matrimony/view/whatsapp_list_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isPasswordVisible = false;
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool isRemeberMe = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: SharedPreferences.getInstance(),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            nameController.text = snapshot.data!.getString('UserName') != null
                ? snapshot.data!.getString('UserName').toString()
                : '';
            passwordController.text =
                snapshot.data!.getString('Password') != null
                    ? snapshot.data!.getString('Password').toString()
                    : '';
            return Stack(
              fit: StackFit.expand,
              children: [
                Image.asset(
                  fit: BoxFit.fitWidth,
                  'assets/images/bg_matrimony_prelogin.jpg',
                ),
                Container(color: Colors.black54),
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(),
                    ),
                    Expanded(
                      flex: 8,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Hello There!\nWelcome Back',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 25,
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 15),
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            color: Colors.white,
                            child: TextFormField(
                              controller: nameController,
                              decoration: InputDecoration(
                                hintText: 'Enter UserName',
                                prefixIcon: const Icon(
                                  Icons.supervised_user_circle,
                                ),
                                suffixIcon: InkWell(
                                  onTap: () {
                                    setState(
                                      () {
                                        nameController.clear();
                                      },
                                    );
                                  },
                                  child: const Icon(
                                    Icons.clear,
                                    size: 15,
                                  ),
                                ),
                              ),
                              style: const TextStyle(
                                fontSize: 16,
                                color: Colors.black,
                              ),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            color: Colors.white,
                            child: TextFormField(
                              controller: passwordController,
                              obscureText: isPasswordVisible,
                              obscuringCharacter: '?',
                              decoration: InputDecoration(
                                hintText: 'Enter Password',
                                prefixIcon: const Icon(Icons.lock),
                                suffixIcon: InkWell(
                                  onTap: () {
                                    setState(
                                      () {
                                        isPasswordVisible = !isPasswordVisible;
                                      },
                                    );
                                  },
                                  child: Icon(
                                    isPasswordVisible
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                  ),
                                ),
                              ),
                              style: const TextStyle(
                                fontSize: 16,
                                color: Colors.black,
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              setState(
                                () {
                                  isRemeberMe = !isRemeberMe;
                                },
                              );
                            },
                            child: Row(
                              children: [
                                Checkbox(
                                  value: isRemeberMe,
                                  checkColor: Colors.pink,
                                  fillColor:
                                      MaterialStatePropertyAll(Colors.white),
                                  onChanged: (value) {
                                    print('CHECKED VALUE :: $value');
                                  },
                                ),
                                Text(
                                  'Remember Me',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(top: 10, bottom: 10),
                            color: Colors.pink,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                TextButton(
                                  onPressed: () {
                                    setLoginCredentials();
                                  },
                                  child: Text(
                                    'Login'.toUpperCase(),
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              TextButton(
                                onPressed: () {},
                                child: const Text(
                                  'Forgot Password?',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Container(),
                      flex: 1,
                    ),
                  ],
                )
              ],
            );
          } else {
            return CircularProgressIndicator();
          }
        },
      ),
    );
  }

  Future<void> setLoginCredentials() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool('IsLogin', isRemeberMe);
    preferences.setString('UserName', nameController.value.text.toString());
    preferences.setString('Password', passwordController.value.text.toString());
    // db.insertNameInTblDemo(name: nameController.value.text.toString());
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) {
          return WhatsappListPage();
        },
      ),
    );
  }
}
