import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:matrimony/view/add_grocery_page.dart';
import 'package:matrimony/view/login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GroceryListPage extends StatefulWidget {
  @override
  State<GroceryListPage> createState() => _GroceryListPageState();
}

class _GroceryListPageState extends State<GroceryListPage> {
  List<Map<String, dynamic>> userList = [];

  @override
  void initState() {
    super.initState();
    setWhatsappItem(counter: 0, name: 'Mehul', message: 'Message Raj');
    setWhatsappItem(counter: 1, name: 'Raj', message: 'Message From Mehul');
  }

  void setWhatsappItem(
      {required String name, required String message, required int counter}) {
    Map<String, dynamic> whatsappItem = {};
    whatsappItem['ImageUrl'] =
        'https://www.pngitem.com/pimgs/m/130-1300380_female-user-image-icon-hd-png-download.png';
    whatsappItem['UserName'] = name;
    whatsappItem['Message'] = message;
    whatsappItem['Counter'] = counter;
    userList.add(whatsappItem);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Whats App'),
        backgroundColor: Colors.green,
        actions: [
          InkWell(
            child: Icon(
              Icons.add,
              color: Colors.white,
            ),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return LoginPage();
                  },
                ),
              ).then((value) {
                setState(() {
                  Map<String, dynamic> map = value;
                  userList.add(map);
                });
              });
            },
          ),
          SizedBox(
            width: 15,
          ),
          InkWell(
            child: Icon(
              Icons.logout,
              color: Colors.white,
            ),
            onTap: () {
              showDialog(
                context: context,
                builder: (context) {
                  return CupertinoAlertDialog(
                    title: null,
                    content: Text('Are you sure want to logout?'),
                    actions: [
                      TextButton(
                        child: Text('Yes'),
                        onPressed: () async {
                          SharedPreferences preference =
                              await SharedPreferences.getInstance();
                          preference.setBool('IsLogin', false);
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(
                              builder: (context) {
                                return LoginPage();
                              },
                            ),
                          );
                        },
                      ),
                      TextButton(
                        child: Text('No'),
                        onPressed: () {},
                      )
                    ],
                  );
                },
              );
            },
          ),
          SizedBox(
            width: 15,
          ),
        ],
      ),
      body: ListView.builder(
        itemCount: userList.length,
        itemBuilder: (context, index) {
          Map<String, dynamic> map = userList[index];
          return InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return AddGroceryPage(
                      counter: map['Counter'],
                      message: map['Message'],
                      name: map['UserName'],
                      imageUrl: map['ImageUrl'],
                    );
                  },
                ),
              ).then((value) {
                setState(() {
                  Map<String, dynamic> map = value;
                  userList[index] = map;
                });
              });
            },
            child: Card(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    CircleAvatar(
                      foregroundImage: NetworkImage(map['ImageUrl']),
                      backgroundColor: Colors.redAccent,
                    ),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(left: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              map['UserName'],
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              map['Message'],
                              style: TextStyle(
                                color: Colors.black45,
                                fontSize: 15,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    map['Counter'] > 0
                        ? Container(
                            padding: EdgeInsets.all(8),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.green,
                            ),
                            child: Text(
                              map['Counter'].toString(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15),
                            ),
                          )
                        : Container(),
                    InkWell(
                      child: Icon(
                        Icons.delete,
                        color: Colors.red,
                      ),
                      onTap: () {
                        setState(() {
                          userList.removeAt(index);
                        });
                      },
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
