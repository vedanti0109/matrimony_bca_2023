import 'package:flutter/material.dart';
import 'package:matrimony/database/demo_database.dart';
import 'package:matrimony/view/add_user.dart';
import 'package:matrimony/view/login_page.dart';
import 'package:matrimony/view/my_database.dart';
import 'package:matrimony/view/student_list_page.dart';
import 'package:matrimony/view/user_list.dart';
import 'package:matrimony/view/whatsapp_list_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  DemoDatabase db = DemoDatabase();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Matrimony',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        home: FutureBuilder(
          future: db.getDatabaseReference(),
          builder: (context, snapshot) {
            if (snapshot.hasData && snapshot.data != null) {
              return FutureBuilder<SharedPreferences>(
                future: SharedPreferences.getInstance(),
                builder: (context, snapshot1) {
                  if (snapshot1.hasData && snapshot1.data != null) {
                    if (snapshot1.data!.getBool('IsLogin') ?? false) {
                      return StudentListPage();
                    } else {
                      return LoginPage();
                    }
                  } else {
                    return const CircularProgressIndicator();
                  }
                },
              );
            } else {
              return CircularProgressIndicator();
            }
          },
        ));
  }
}
